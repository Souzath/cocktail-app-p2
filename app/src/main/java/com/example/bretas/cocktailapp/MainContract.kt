package com.example.bretas.cocktailapp

import android.content.Context

interface MainContract {

    interface View{
        fun showMessage(msg: String)
        fun showList(drinks: List<Drink>)
        fun showLoading()
        fun hideLoading()
    }

    interface Presenter {
        fun onLoadList(context: Context)
        fun onLoadListRandom(context: Context)
    }

}