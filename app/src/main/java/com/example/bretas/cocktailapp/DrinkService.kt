package com.example.bretas.cocktailapp

import com.example.bretas.cocktailapp.DrinkList
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface DrinkService {


    @GET("random.php")
    fun getRandomDrink(): Call<DrinkList>

    @GET("filter.php")
    fun getAlcoholic(@Query("a") a: String = "Alcoholic"): Call<DrinkList>


}