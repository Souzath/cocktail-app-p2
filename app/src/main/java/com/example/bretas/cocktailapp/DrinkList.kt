package com.example.bretas.cocktailapp

data class DrinkList (val drinks: List<Drink>)