package com.example.bretas.cocktailapp

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import java.io.Serializable

@Entity
data class Drink(var strDrink: String,
                 var strInstructions: String,
                 var strDrinkThumb: String,
                 @PrimaryKey(autoGenerate = true)
                 var idDrink: Int = 0) : Serializable

