package com.example.bretas.cocktailapp

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.example.bretas.cocktailapp.GlideApp
import com.example.bretas.cocktailapp.R
import com.example.bretas.cocktailapp.Drink
import kotlinx.android.synthetic.main.activity_details.*

class DetailsActivity : AppCompatActivity() {

    companion object {
        public const val DRINK: String = "Drink" //para putExtra entre activities

    }

    var drink: Drink? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)

        //verifica se foi passado um drink para alteração
        drink = intent.getSerializableExtra(DRINK) as Drink?
        if(drink != null){
            carregaDados()
        }
    }

    //exibe as informações do drink na Activity
    private fun carregaDados() {
        drinkName.setText("Name: " + drink?.strDrink)
        drinkId.setText("ID: " + drink?.idDrink)

        if(drink?.strInstructions != null) {
            drinkInstructions.setText("Instructions: " + drink?.strInstructions)
        }

        GlideApp.with(this)
            .load(drink?.strDrinkThumb)
            .placeholder(R.drawable.no_image)
            .centerCrop()
            .into(imgDrinks)

    }

}